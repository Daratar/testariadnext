package com.ariadnext.technicaltest.socialconnection;

import org.springframework.social.connect.Connection;
import org.springframework.social.connect.ConnectionSignUp;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class FacebookConnectionSignup implements ConnectionSignUp {

    @Override
    public String execute(final Connection<?> connection) {
        // The best here would be to persist the user
        // But getting the name will be enough
        return connection.getDisplayName();
    }
}
