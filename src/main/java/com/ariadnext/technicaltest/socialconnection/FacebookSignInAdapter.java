package com.ariadnext.technicaltest.socialconnection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.social.connect.Connection;
import org.springframework.social.connect.web.SignInAdapter;
import org.springframework.web.context.request.NativeWebRequest;

import java.util.Arrays;

public class FacebookSignInAdapter implements SignInAdapter {
    @Override
    public String signIn(final String localUserId, final Connection<?> connection, final NativeWebRequest request) {

        // On authentication, just set up the user from Facebook into the Security Context
        // Using the persisted user would be the best thing to do
        SecurityContextHolder.getContext().setAuthentication(
                new UsernamePasswordAuthenticationToken(
                        connection.getDisplayName(), null,
                        Arrays.asList(new SimpleGrantedAuthority("FACEBOOK_USER"))));

        return null;
    }
}
