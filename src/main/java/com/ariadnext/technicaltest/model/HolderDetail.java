package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class HolderDetail {

    private List<String> lastName;
    private List<String> firstName;
    private String usageName;
    private String nationality;
    private Gender gender;
    private String address;
    private Date birthDate;
    private String birthPlace;
    private String birthDpt;
    private ExtraInfo extraInfos;

    public List<String> getLastName() {
        return lastName;
    }

    public void setLastName(List<String> lastName) {
        this.lastName = lastName;
    }

    public List<String> getFirstName() {
        return firstName;
    }

    public void setFirstName(List<String> firstName) {
        this.firstName = firstName;
    }

    public String getUsageName() {
        return usageName;
    }

    public void setUsageName(String usageName) {
        this.usageName = usageName;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public String getBirthPlace() {
        return birthPlace;
    }

    public void setBirthPlace(String birthPlace) {
        this.birthPlace = birthPlace;
    }

    public String getBirthDpt() {
        return birthDpt;
    }

    public void setBirthDpt(String birthDpt) {
        this.birthDpt = birthDpt;
    }

    public ExtraInfo getExtraInfos() {
        return extraInfos;
    }

    public void setExtraInfos(ExtraInfo extraInfos) {
        this.extraInfos = extraInfos;
    }
}
