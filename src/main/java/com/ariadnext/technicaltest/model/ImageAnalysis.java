package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImageAnalysis {

    String frontImage;
    Image backImage;
    Boolean rectoImageCropped;
    Boolean signatureImageCropped;
    Boolean faceImageCropped;

    public ImageAnalysis() {}

    public ImageAnalysis(String frontImage) {
        this.frontImage = frontImage;
    }

    public String getFrontImage() {
        return frontImage;
    }

    public void setFrontImage(String frontImage) {
        this.frontImage = frontImage;
    }

    public Image getBackImage() {
        return backImage;
    }

    public void setBackImage(Image backImage) {
        this.backImage = backImage;
    }

    public Boolean getRectoImageCropped() {
        return rectoImageCropped;
    }

    public void setRectoImageCropped(Boolean rectoImageCropped) {
        this.rectoImageCropped = rectoImageCropped;
    }

    public Boolean getSignatureImageCropped() {
        return signatureImageCropped;
    }

    public void setSignatureImageCropped(Boolean signatureImageCropped) {
        this.signatureImageCropped = signatureImageCropped;
    }

    public Boolean getFaceImageCropped() {
        return faceImageCropped;
    }

    public void setFaceImageCropped(Boolean faceImageCropped) {
        this.faceImageCropped = faceImageCropped;
    }
}
