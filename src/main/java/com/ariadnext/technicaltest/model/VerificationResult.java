package com.ariadnext.technicaltest.model;

public enum VerificationResult {

    OK("verification passed"),
    WARNING("problems occured"),
    ERROR("verification detected a problem"),
    NONE("verification skipped");

    private String label;

    VerificationResult(String label) {
        this.label = label;
    }

    public String getLabel() {
            return label;
        }
}
