package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentClassification {

    private DocumentClassificationIdType idType;

    public DocumentClassificationIdType getIdType() {
        return idType;
    }

    public void setIdType(DocumentClassificationIdType idType) {
        this.idType = idType;
    }

    public enum DocumentClassificationIdType {
        ID("National identity card"),
        P("Passport"),
        V("Visa"),
        RP("Residence permit"),
        UNKNOWN("unknown type");

        private String label;

        DocumentClassificationIdType(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }
}
