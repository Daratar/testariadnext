package com.ariadnext.technicaltest.model;

public enum Gender {
    M("male"),
    F("female"),
    X("unknown");

    private String value;

    Gender(String value) {
        this.value = value;
    }

    public String getLabel() {
        return value;
    }
}