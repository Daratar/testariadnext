package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class ExtractedImage {

    private ImageType type;
    private String imageDL;

    public ImageType getType() {
        return type;
    }

    public void setType(ImageType type) {
        this.type = type;
    }

    public String getImageDL() {
        return imageDL;
    }

    public void setImageDL(String imageDL) {
        this.imageDL = imageDL;
    }
}
