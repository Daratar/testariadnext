package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class SingleVerification {
    private SingleVerificationIdentifier identifier;
    private String titleMsg;
    private String resultMsg;
    private VerificationResult result;

    public SingleVerificationIdentifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(SingleVerificationIdentifier identifier) {
        this.identifier = identifier;
    }

    public String getTitleMsg() {
        return titleMsg;
    }

    public void setTitleMsg(String titleMsg) {
        this.titleMsg = titleMsg;
    }

    public String getResultMsg() {
        return resultMsg;
    }

    public void setResultMsg(String resultMsg) {
        this.resultMsg = resultMsg;
    }

    public VerificationResult getResult() {
        return result;
    }

    public void setResult(VerificationResult result) {
        this.result = result;
    }

    public enum SingleVerificationIdentifier {
        SUMMARY_ID_IDENTIFIED("document identification"),
        SUMMARY_ID_EXPIRED("expiry verification"),
        SUMMARY_ID_FALSIFIED("falsification detection"),
        SUMMARY_ID_SPECIMEN("specimen detection"),
        SUMMARY_ID_COPY("detection");

        private String label;

        SingleVerificationIdentifier(String label) {
            this.label = label;
        }

        public String getLabel() {
            return label;
        }
    }

}
