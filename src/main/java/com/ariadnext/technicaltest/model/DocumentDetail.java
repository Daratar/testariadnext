package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class DocumentDetail {

    private String emitCountry;
    private Date emitDate;
    private Date expirationDate;
    private String documentNumber;
    private ExtraInfo extraInfos;

    public String getEmitCountry() {
        return emitCountry;
    }

    public void setEmitCountry(String emitCountry) {
        this.emitCountry = emitCountry;
    }

    public Date getEmitDate() {
        return emitDate;
    }

    public void setEmitDate(Date emitDate) {
        this.emitDate = emitDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public ExtraInfo getExtraInfos() {
        return extraInfos;
    }

    public void setExtraInfos(ExtraInfo extraInfos) {
        this.extraInfos = extraInfos;
    }
}
