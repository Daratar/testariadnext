package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class AnalysisResult {

    private String uid;
    private String analysisRefUid;
    private Check checkReportSummary;
    private DocumentClassification documentClassification;
    private DocumentDetail documentDetail;
    private HolderDetail holderDetail;
    private MRZ mrz;
    private List<ControlGroup> controls;
    private List<ExtractedImage> image;

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getAnalysisRefUid() {
        return analysisRefUid;
    }

    public void setAnalysisRefUid(String analysisRefUid) {
        this.analysisRefUid = analysisRefUid;
    }

    public Check getCheckReportSummary() {
        return checkReportSummary;
    }

    public void setCheckReportSummary(Check checkReportSummary) {
        this.checkReportSummary = checkReportSummary;
    }

    public DocumentClassification getDocumentClassification() {
        return documentClassification;
    }

    public void setDocumentClassification(DocumentClassification documentClassification) {
        this.documentClassification = documentClassification;
    }

    public DocumentDetail getDocumentDetail() {
        return documentDetail;
    }

    public void setDocumentDetail(DocumentDetail documentDetail) {
        this.documentDetail = documentDetail;
    }

    public HolderDetail getHolderDetail() {
        return holderDetail;
    }

    public void setHolderDetail(HolderDetail holderDetail) {
        this.holderDetail = holderDetail;
    }

    public MRZ getMrz() {
        return mrz;
    }

    public void setMrz(MRZ mrz) {
        this.mrz = mrz;
    }

    public List<ControlGroup> getControls() {
        return controls;
    }

    public void setControls(List<ControlGroup> controls) {
        this.controls = controls;
    }

    public List<ExtractedImage> getImage() {
        return image;
    }

    public void setImage(List<ExtractedImage> image) {
        this.image = image;
    }
}
