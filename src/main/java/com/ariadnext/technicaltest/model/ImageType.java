package com.ariadnext.technicaltest.model;

public enum ImageType {
    CROPPED_RECTO("cropped document"),
    CROPPED_FACE("hoder's face"),
    CROPPED_SIGNATURE("holder's signature");

    private String value;

    ImageType(String value) {
        this.value = value;
    }

    public String getLabel() {
        return value;
    }
}
