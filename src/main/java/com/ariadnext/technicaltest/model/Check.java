package com.ariadnext.technicaltest.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Check {
    private List<SingleVerification> check;

    public List<SingleVerification> getCheck() {
        return check;
    }

    public void setCheck(List<SingleVerification> check) {
        this.check = check;
    }
}
