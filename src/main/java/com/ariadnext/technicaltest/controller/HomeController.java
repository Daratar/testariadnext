package com.ariadnext.technicaltest.controller;

import com.ariadnext.technicaltest.business.ImageBusiness;
import com.ariadnext.technicaltest.model.AnalysisResult;
import com.ariadnext.technicaltest.model.Image;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.util.Base64;
import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private ImageBusiness imageBusiness;

    @GetMapping("/")
    public String homepageAction(final Model model) throws JsonProcessingException {

        List<Image> imageList = imageBusiness.buildImageList();

        model.addAttribute("imageList", imageList);

        return "homepage";
    }

    @PostMapping("/validateId")
    public String validateIdAction(@RequestParam("file") MultipartFile file, RedirectAttributes attributes) throws IOException {

        // check if file is empty
        if (!file.isEmpty()) {
            byte[] fileContent = file.getBytes();
            String encodedString = Base64.getEncoder().encodeToString(fileContent);

            Image image = new Image();
            image.setBase64Image(encodedString);

            AnalysisResult analysisResult = imageBusiness.createTaskImage(image);
            attributes.addFlashAttribute("success", "Task " + analysisResult.getUid()
                    + " created and finished successfully.");
        }

        return "redirect:/";
    }

}
