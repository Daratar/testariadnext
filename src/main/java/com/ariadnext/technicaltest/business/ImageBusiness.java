package com.ariadnext.technicaltest.business;

import com.ariadnext.technicaltest.model.AnalysisResult;
import com.ariadnext.technicaltest.model.Image;
import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.List;

public interface ImageBusiness {

    /**
     * Build the image list with available images
     * Add one fake image to test error case
     *
     * @return List<Image>
     */
    List<Image> buildImageList() throws JsonProcessingException;

    /**
     * Create a task image to start an analysis
     *
     * @param image The image to check
     * @return
     */
    AnalysisResult createTaskImage(Image image) throws JsonProcessingException;
}
