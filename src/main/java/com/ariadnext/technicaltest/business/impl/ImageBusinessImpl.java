package com.ariadnext.technicaltest.business.impl;

import com.ariadnext.technicaltest.business.ImageBusiness;
import com.ariadnext.technicaltest.model.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Component
public class ImageBusinessImpl implements ImageBusiness {

    @Value("${spring.checkio.base_url.sandbox}")
    String checkIoBaseUrlSandbox;

    @Value("${spring.checkio.auth.username}")
    String checkIoAuthUsername;

    @Value("${spring.checkio.auth.password}")
    String checkIoAuthPassword;

    @Autowired
    RestTemplate restTemplate;

    @Override
    public List<Image> buildImageList() throws JsonProcessingException {

        List<Image> imageList = new ArrayList<>();

        ImageListResponse imageListResponse = getImageListResponse();

        if (null != imageListResponse && null != imageListResponse.getImages()
                && !imageListResponse.getImages().isEmpty()) {
            imageListResponse.getImages().forEach(image -> {
                // Grab one image for test, getting the FR and BASE64 one
                if ("BASE64".equals(image.getRawType())
                        && "RECTO".equals(image.getFace())) {
                    try {
                        String base64Image = getBase64FromImage(image);
                        image.setBase64Image(base64Image);
                        imageList.add(image);
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
            });
        }

        return imageList;
    }

    /**
     * GEt the list of available images in sandbox mode
     *
     * @return ImageListResponseDto
     * @throws JsonProcessingException Json not formatted
     */
    private ImageListResponse getImageListResponse() throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();

        String uri = "/v0/sandbox/imagelist";
        String jsonResponse = restTemplate.getForObject(checkIoBaseUrlSandbox + uri, String.class);
        return objectMapper.readValue(jsonResponse, ImageListResponse.class);
    }

    /**
     * Get de Base64 image String to display the image
     *
     * @param image The image to get the BASE64 for
     * @return String The Base64
     * @throws JsonProcessingException Json not formatted
     */
    private String getBase64FromImage(final Image image) throws JsonProcessingException {
        String uri = "/v0/sandbox/image";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(checkIoBaseUrlSandbox + uri)
                .pathSegment(image.getDoc())
                .queryParam("rawType", "BASE64")
                .queryParam("face", image.getFace())
                .queryParam("light", "DL");
        UriComponents components = builder.build(true);

        return restTemplate.getForObject(components.toUriString(), String.class);
    }

    public AnalysisResult createTaskImage(Image image) throws JsonProcessingException {
        String uri = "/v0/task/image";

        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(checkIoBaseUrlSandbox + uri)
                .queryParam("asyncMode", "false");
        UriComponents components = builder.build(true);

        ImageAnalysis imageAnalysis = new ImageAnalysis(image.getBase64Image());

        ObjectMapper objectMapper = new ObjectMapper();
        String jsonImageAnalysis = objectMapper.writeValueAsString(imageAnalysis);

        HttpEntity<String> request = new HttpEntity<>(jsonImageAnalysis, createHeaders());

        String jsonResponse = restTemplate.postForObject(components.toUriString(), request, String.class);

        return objectMapper.readValue(jsonResponse, AnalysisResult.class);
    }

    private HttpHeaders createHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();

        String auth = checkIoAuthUsername + ":" + checkIoAuthPassword;
        byte[] encodedAuth = Base64.encodeBase64(auth.getBytes(StandardCharsets.US_ASCII));

        String authHeader = "Basic " + new String( encodedAuth );
        httpHeaders.set( "Authorization", authHeader );
        httpHeaders.setContentType(MediaType.APPLICATION_JSON);

        return httpHeaders;
    }
}
