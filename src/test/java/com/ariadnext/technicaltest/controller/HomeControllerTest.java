package com.ariadnext.technicaltest.controller;

import com.ariadnext.technicaltest.business.ImageBusiness;
import com.ariadnext.technicaltest.model.Image;
import com.ariadnext.technicaltest.socialconnection.FacebookConnectionSignup;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@ContextConfiguration(classes = {FacebookConnectionSignup.class, ImageBusiness.class})
@Import(HomeController.class)
public class HomeControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ImageBusiness imageBusiness;

    @Test
    public void homeShouldReturnUnauthorized() throws Exception {
        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isUnauthorized());
    }

    @Test
    @WithMockUser
    public void homeShouldReturnAuthorized() throws Exception {
        List<Image> imageList = new ArrayList<>();
        Mockito.when(imageBusiness.buildImageList()).thenReturn(imageList);

        this.mockMvc.perform(get("/")).andDo(print()).andExpect(status().isOk());
    }
}
