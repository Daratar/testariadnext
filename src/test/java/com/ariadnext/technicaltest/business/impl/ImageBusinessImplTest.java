package com.ariadnext.technicaltest.business.impl;

import com.ariadnext.technicaltest.model.Image;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

@SpringBootTest
public class ImageBusinessImplTest {

    @Autowired
    private ImageBusinessImpl imageBusinessImpl;

    @MockBean
    private RestTemplate restTemplate;

    @Test
    public void buildImageListWithoutImage() throws Exception {
        String uriForImageList = "https://sandbox.idcheck.io/rest/v0/sandbox/imagelist";
        String expectedResponse = "{\"images\":[]}";
        Mockito.when(restTemplate.getForObject(uriForImageList, String.class)).thenReturn(expectedResponse);

        List<Image> imageList = imageBusinessImpl.buildImageList();
        assertThat(imageList, hasSize(0));
    }

    @Test
    public void buildImageListWithNoGoodImage() throws Exception {
        String uriForImageList = "https://sandbox.idcheck.io/rest/v0/sandbox/imagelist";
        String expectedResponse = "{\"images\":["
                + "{\"doc\":\"CNI_FR_SPECIMEN_BERTHIER\",\"face\":\"RECTO\",\"rawType\":\"JPG\",\"light\":\"DL\"},"
                + "{\"doc\":\"CNI_FR_SPECIMEN_BERTHIER\",\"face\":\"VERSO\",\"rawType\":\"BASE64\",\"light\":\"DL\"}"
                + "]}";
        Mockito.when(restTemplate.getForObject(uriForImageList, String.class)).thenReturn(expectedResponse);

        List<Image> imageList = imageBusinessImpl.buildImageList();
        assertThat(imageList, hasSize(0));
    }

    @Test
    public void buildImageListWithImage() throws Exception {
        String uriForImageList = "https://sandbox.idcheck.io/rest/v0/sandbox/imagelist";
        String expectedResponse = "{\"images\":["
            + "{\"doc\":\"CNI_FR_SPECIMEN_BERTHIER\",\"face\":\"RECTO\",\"rawType\":\"BASE64\",\"light\":\"DL\"},"
            + "{\"doc\":\"PASSEPORT_CHN_SPECIMEN_ZHENGJIAN\",\"face\":\"RECTO\",\"rawType\":\"BASE64\",\"light\":\"DL\"}"
            + "]}";
        Mockito.when(restTemplate.getForObject(uriForImageList, String.class)).thenReturn(expectedResponse);

        List<Image> imageList = imageBusinessImpl.buildImageList();
        assertThat(imageList, hasSize(2));
    }
}
